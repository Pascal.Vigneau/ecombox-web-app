import { RouterModule, Routes, ExtraOptions} from '@angular/router';
import { NgModule } from '@angular/core';

const config: ExtraOptions = {
  useHash: true,
};

const routes: Routes = [
  {
    path: 'ecombox',
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    loadChildren: ()  => import('./ecombox/ecombox.module')
    .then(m => m.EcomboxModule),
  },
  { path: '', redirectTo: 'ecombox', pathMatch: 'full' },
  { path: '**', redirectTo: 'ecombox' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
