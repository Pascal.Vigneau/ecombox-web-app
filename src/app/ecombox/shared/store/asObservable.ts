import { Subject, Observable } from "rxjs";
import { Stack } from "../models/stack";

/**
 * Cast a Subject to an Observable
 * @param {Subject} subject
 * @return {Observable}
 */
export function asObservable(subject: Subject<Stack[]>): Observable<Stack[]> {
  return new Observable((fn) => subject.subscribe(fn));
}
