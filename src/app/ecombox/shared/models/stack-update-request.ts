import { StackEnv } from "./stack-env";

export interface StackUpdateRequest {
  /**
   * New content of the Stack file.
   */
  stackFileContent?: string;
  /**
   * A list of environment variables used during stack deployment
   */
  env?: Array<StackEnv>;
  /**
   * Prune services that are no longer referenced (only available for Swarm stacks)
   */
  prune?: boolean;
}
