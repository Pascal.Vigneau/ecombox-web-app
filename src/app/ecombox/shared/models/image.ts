import { Injectable } from "@angular/core";
import { Adapter } from "./adapter";

export class Image {
  Size?: number;
}

@Injectable({
  providedIn: "root",
})
export class ImageAdapter implements Adapter<Image> {
  /**
   * Create new container from json object
   * @param {any} item
   * @return {Container}
   */
  adapt(item): Image {
    return new Image();
  }
}
