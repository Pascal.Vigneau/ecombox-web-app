import { Container } from "@angular/compiler/src/i18n/i18n_ast";
import { Image } from "./image";

export interface DataUsage {
  Images?: Array<Image>;
  Containers?: Array<Container>;
}
