import { NbMenuItem } from "@nebular/theme";
import { SERVER_TYPES } from "./shared/api/variables";

export const MENU_ECOMBOX_ITEMS: NbMenuItem[] = [
  {
    title: "Tableau de bord",
    icon: { icon: "home", pack: "fa" },
    link: "/ecombox/dashboard",
    home: true,
  },
  {
    title: "Prestashop",
    icon: { icon: "store", pack: "fa" },
    link: `/ecombox/servers/${SERVER_TYPES.prestashop}`,
  },
  {
    title: "WooCommerce",
    icon: { icon: "shopping-cart", pack: "fa" },
    link: `/ecombox/servers/${SERVER_TYPES.woocommerce}`,
  },
  {
    title: "Blog - WordPress",
    icon: { icon: "blog", pack: "fa" },
    link: `/ecombox/servers/${SERVER_TYPES.blog}`,
  },
  {
    title: "Mautic",
    icon: { icon: "poll", pack: "fa" },
    link: `/ecombox/servers/${SERVER_TYPES.mautic}`,
  },
  {
    title: "Suite CRM",
    icon: { icon: "people-arrows", pack: "fa" },
    link: `/ecombox/servers/${SERVER_TYPES.suitecrm}`,
  },
  {
    title: "Odoo",
    icon: { icon: "project-diagram", pack: "fa" },
    link: `/ecombox/servers/${SERVER_TYPES.odoo}`,
  },
  {
    title: "Kanboard",
    icon: { icon: "tasks", pack: "fa" },
    link: `/ecombox/servers/${SERVER_TYPES.kanboard}`,
  },
  {
    title: "HumHub",
    icon: { icon: "users", pack: "fa" },
    link: `/ecombox/servers/${SERVER_TYPES.humhub}`,
  },
  {
    title: "Gestion avancée",
    icon: { icon: "exclamation-triangle", pack: "fa" },
    children: [
      {
        title: "Accès SFTP",
        link: `/ecombox/servers/${SERVER_TYPES.sftp}`,
      },
      {
        title: "Accès phpMyAdmin",
        link: `/ecombox/servers/${SERVER_TYPES.pma}`,
      },
      {
        title: "Accès admin",
        link: "/ecombox/portainer",
      },
      {
        title: "Gérer vos modèles de sites",
        link: "/ecombox/images",
      },
    ],
  },
  {
    title: "Aide",
    icon: { icon: "question-circle", pack: "fa" },
    link: "/ecombox/help",
  },
  /* TODO in 4.0.0 version{
    title: "Mention légale et confidentialité",
    icon: { icon: "balance-scale", pack: "fa" },
    link: "/ecombox/legal-notice-privacy-policy",
  },*/
];
