import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NbMenuModule } from "@nebular/theme";
import { ThemeModule } from "../@theme/theme.module";
import { EcomboxRoutingModule } from "./ecombox-routing.module";
import { MiscellaneousModule } from "./miscellaneous/miscellaneous.module";
import { SharedModule } from "./shared/shared.module";
import { DashboardModule } from "./dashboard/dashboard.module";
import { ImagesModule } from "./images/images.module";
import { RedirectGuard } from "./redirect-guard";

import { EcomboxComponent } from "./ecombox.component";
import { HelpComponent } from "./help/help.component";
import { PolicyModule } from "./policies/policies.module";

@NgModule({
  declarations: [EcomboxComponent, HelpComponent],
  imports: [
    CommonModule,
    EcomboxRoutingModule,
    MiscellaneousModule,
    NbMenuModule,
    ThemeModule,
    SharedModule,
    DashboardModule,
    ImagesModule,
    PolicyModule,
  ],
  providers: [RedirectGuard]
})
export class EcomboxModule {}
