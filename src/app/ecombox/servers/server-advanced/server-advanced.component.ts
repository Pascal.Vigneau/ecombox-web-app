import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import { SERVER_TYPES } from "../../shared/api/variables";
import { Stack } from "../../shared/models/stack";
import { StackStore } from "../../shared/store/stack.store";

interface CardSettings {
  title: string;
  iconClass: string;
  id: string;
  type: string;
  on: boolean;
  actif: string;
  url: string;
  typeContainer: string;
  nameStackAss: string;
  idStackAss: number;
  typeStackAss: string;
}

interface ControlsMap<T extends Array<CardSettings>> {
  [key: string]: T;
}

@Component({
  selector: "ecx-server-advanced",
  templateUrl: "./server-advanced.component.html",
  styleUrls: ["./server-advanced.component.scss"],
})
export class ServerAdvancedComponent implements OnInit, OnDestroy {
  serverType: string;
  title: string;
  warning: string;
  display: boolean;
  loading: false;
  test: ControlsMap<Array<CardSettings>> = {};

  stacksList: Array<Stack> = [];
  commonStatusCardsSet: Array<CardSettings> = [];
  statusCardsByThemes: {
    default: Array<CardSettings>;
  };
  private subscription: Subscription;

  /**
   *
   * @param {StackStore} stackStore
   */
  constructor(
    private stackStore: StackStore,
    private actRoute: ActivatedRoute
  ) {
    this.serverType = this.actRoute.snapshot.url.toString();
    if (this.serverType === SERVER_TYPES.pma) {
      this.title = "Gestion de l'accès à phpMyAdmin (PMA)";
      this.warning = `Les sites listés sur cette page sont les sites Prestashop, WooCommerce et Blog actuellement démarrés
	   car cela est obligatoire pour pouvoir activer phpMyAdmin.`;
    } else if (this.serverType === SERVER_TYPES.sftp) {
      this.title = "Gestion du SFTP";
      this.warning = `Les sites listés sur cette page sont les sites Prestashop et Odoo actuellement démarrés 
		car cela est obligatoire pour pouvoir activer le SFTP et accéder aux sources.`;
    }
  }

  /**
   *
   */
  ngOnInit(): void {
    this.subscription = this.stackStore.getStacks().subscribe((stacks) => {
      this.stacksList = [];
      this.test = {};
      // this.commonStatusCardsSet = [];
      stacks.forEach((stack) => {
        switch (this.serverType) {
          case SERVER_TYPES.sftp:
            if (
              stack.getServerType() &&
              (stack.getServerType() === SERVER_TYPES.prestashop ||
                stack.getServerType() === SERVER_TYPES.odoo) &&
              stack.getStatus() === 1
            ) {
              this.displayCards(stack);
            }
            break;
          case SERVER_TYPES.pma:
            if (
              stack.getStatus() === 1 &&
              stack.getServerType() &&
              (stack.getServerType() === SERVER_TYPES.prestashop ||
                stack.getServerType() === SERVER_TYPES.woocommerce ||
                stack.getServerType() === SERVER_TYPES.blog)
            ) {
              this.displayCards(stack);
            }
            break;
        }
      });
    });
  }

  /**
   * @param {Stack} stack
   */
  private displayCards(stack: Stack): void {
    let state: boolean = false;
    let info: string = "";
    let idAss: number;
    let nameAss: string = "";
    // We check if an associate sftp or pma stack exists
    const stackAssociate = this.stackStore.getStackAssociate(stack.getName());

    if (
      stackAssociate.length > 0 &&
      stackAssociate[0].getContainers() !== undefined
    ) {
      stackAssociate.forEach((s) => {
        if (
          this.serverType === SERVER_TYPES.sftp &&
          s.getName().startsWith("sftp")
        ) {
          state = true;
          info = this.getConnectInfo(
            s.getContainers()[0].getPort(),
            s.getEnvVars().find((e) => e.name === "SFTP_PASS").value
          );
          nameAss = s.getName();
          idAss = s.getID();
          return;
        } else if (
          this.serverType === SERVER_TYPES.pma &&
          s.getName().startsWith("pma")
        ) {
          state = true;
          info = this.getConnectInfo(
            s.getContainers()[0].getPort(),
            stack.getName()
          );
          nameAss = s.getName();
          idAss = s.getID();
          return;
        }
      });
    }

    // We get suffix of the main stack
    let suffix: string;
    if (stack?.getEnvVars() !== null && stack?.getEnvVars() !== undefined) {
      suffix = stack?.getEnvVars().find((e) => e.name === "SUFFIXE").value;
    }
    this.buildServerCard(
      stack.getServerType() + "-" + suffix,
      stack.getID(),
      state,
      nameAss,
      // CSS style for stack associate status
      state ? "active" : "desactive",
      stack.getServerType(),
      info,
      idAss,
      this.serverType
    );
  }

  /**
   *
   * @param {number} port
   * @param {string} passOrName - Password for SFTP and Name of main stack for PMA
   * @return {string}
   */
  public getConnectInfo(port: number, passOrName: string): string {
    let info: string = "";
    // url: 'http://' + this.ipDocker + ':' + this.portNginx + "/pma-" + containerPresta.Labels["com.docker.compose.project"] + "/",
    switch (this.serverType) {
      case SERVER_TYPES.pma:
        info = `http://${this.stackStore.getDockerIP()}:${this.stackStore.getNginxPort()}/pma-${passOrName}/`;
        break;
      case SERVER_TYPES.sftp:
        info = `Hôte : ${this.stackStore.getDockerIP()} Port : ${port} MDP : ${passOrName}`;
        break;
    }
    return info;
  }

  /**
   *
   * @param {string} title
   * @param {string} stackID
   * @param {boolean} state
   * @param {string} assName
   * @param {string} active
   * @param {string} mainStackType
   * @param {string} info
   * @param {number} idStackAss
   * @param {string} typeStackAss
   */
  buildServerCard(
    title: string,
    stackID: number,
    state: boolean,
    assName: string,
    active: string,
    mainStackType: string,
    info: string,
    idStackAss: number,
    typeStackAss: string
  ): void {
    let icon: string;
    if (typeStackAss === SERVER_TYPES.pma) {
      icon = "nb-power-circled";
    } else if (typeStackAss === SERVER_TYPES.sftp) {
      icon = "nb-angle-double-right";
    }
    const serverCard: CardSettings = {
      title: title,
      iconClass: icon,
      type: "success",
      on: state,
      actif: active,
      id: stackID.toString(),
      nameStackAss: assName,
      typeContainer: mainStackType,
      url: info,
      idStackAss: idStackAss,
      typeStackAss: typeStackAss,
    };

    if (this.test[mainStackType] === undefined) {
      this.test[mainStackType] = [];
    }
    this.test[mainStackType].push(serverCard);
  }

  /**
   *
   */
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
