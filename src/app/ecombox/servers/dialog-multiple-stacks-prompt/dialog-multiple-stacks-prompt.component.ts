import { Component, OnInit } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from "@angular/forms";

@Component({
  selector: "ecx-dialog-multiple-stacks-prompt",
  templateUrl: "dialog-multiple-stacks-prompt.component.html",
  styleUrls: ["dialog-multiple-stacks-prompt.component.scss"],
})
export class DialogMultipleStacksPromptComponent implements OnInit {
  message: string;

  options = [
    { value: "non", label: "Non", checked: true },
    { value: "oui", label: "Oui" },
  ];
  option = "non";

  registerForm: FormGroup;
  submitted = false;

  /**
   *
   * @param {NbDialogRef} ref
   */
  constructor(
    protected ref: NbDialogRef<DialogMultipleStacksPromptComponent>,
    private formBuilder: FormBuilder
  ) {
    this.message =
      "Voulez-vous créer plusieurs sites à partir de cette configuration ?";
  }

  /**
   *
   */
  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      nbOfStacks: [
        "",
        [Validators.required, Validators.min(1), Validators.max(30)],
      ],
    });

    this.getFocus();
  }

  /**
   *
   */
  getFocus(): void {
    document.getElementById("btnSubmit").focus();
  }

  /**
   * convenience getter for easy access to form fields
   */
  get nbStacks(): AbstractControl {
    return this.registerForm.get("nbOfStacks");
  }

  /**
   *
   * @param {string} nb
   */
  onSubmit(): void {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid && this.option === "oui") {
      return;
    }

    let res;
    if (this.option === "non") {
      res = 1;
    } else {
      res = this.nbStacks.value;
    }
    this.ref.close(res);
  }

  /**
   *
   */
  onReset(): void {
    this.submitted = false;
    this.ref.close();
  }

  err: string;

  /**
   *
   */
  cancel(): void {
    this.ref.close();
  }

  /**
   *
   * @param {string} nb
   */
  submit(nb: string): void {
    this.ref.close(nb);
  }
}
