/* eslint-disable no-invalid-this */
import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { NbDialogService } from "@nebular/theme";
import { ToastrService } from "ngx-toastr";
import { Subscription, scheduled, queueScheduler } from "rxjs";
import { concatAll } from "rxjs/operators";
import { SERVER_TYPES } from "../../shared/api/variables";
import { Stack } from "../../shared/models/stack";
import { StackStore } from "../../shared/store/stack.store";
import { DialogNamePromptComponent } from "../dialog-name-prompt/dialog-name-prompt.component";
import { ShowcaseDialogComponent } from "../dialog-pdf/showcase-dialog.component";

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
  stackID: number;
  on: boolean;
}

@Component({
  selector: "ecx-servers-manage",
  templateUrl: "./servers-manage.component.html",
  styleUrls: ["./servers-manage.component.scss"],
})
export class ServersManageComponent implements OnInit, OnDestroy {
  @Input() type: string;
  @Input() custom: boolean;

  noStack: boolean = true;
  stacksList: Array<Stack> = [];
  commonStatusCardsSet: Array<CardSettings> = [];
  statusCardsByThemes: {
    default: Array<CardSettings>;
  };
  private subscription: Subscription;

  /**
   *
   * @param {StackStore} stackStore
   */
  constructor(
    private stackStore: StackStore,
    private toast: ToastrService,
    private dialogService: NbDialogService
  ) {}

  /**
   *
   */
  ngOnInit(): void {
    this.subscription = this.stackStore.getStacks().subscribe((stacks) => {
      this.stacksList = [];
      this.commonStatusCardsSet = [];
      stacks.forEach((stack) => {
        if (stack.getServerType() && stack.getServerType() === this.type) {
          const tag = stack.getEnvVars().find((e) => e.name === "TAG")?.value;
          if (!this.custom) {
            if (tag === undefined || tag === "") {
              this.stacksList.push(stack);
            }
          } else {
            if (tag !== undefined && tag !== "") {
              this.stacksList.push(stack);
            }
          }
        }
      });
      this.stacksList.forEach((stack) => {
        // Check if stack is started
        const state: boolean = stack.getStatus() === 1;

        // Disable actions on stacks if no stack
        this.noStack = this.stacksList.length === 0;

        let suffix: string;
        if (stack?.getEnvVars() !== null && stack?.getEnvVars() !== undefined) {
          suffix = stack?.getEnvVars()[0]?.value;
        }
        this.buildServerCard(
          this.buildNameToDisplay(suffix),
          stack.getID(),
          state
        );
      });
    });
  }

  /**
   * Start all existing stopped stacks
   */
  onStartAll(): void {
    this.toast.info("Tous les sites arrêtés vont être démarrés");
    const requests = [];
    this.stacksList.forEach((stack) => {
      if (stack.getStatus() === 2) {
        requests.push(
          this.stackStore.updateStack(stack.getID(), stack.getServerType())
        );
      }
    });
    // Fix - Send requests sequentially because on some servers there is a bug with unhealthy containers
    scheduled(requests, queueScheduler).pipe(concatAll()).subscribe();
  }

  /**
   * Stop all started stacks
   */
  onStopAll(): void {
    this.toast.info("Tous les sites démarrés vont être arrêtés");
    const requests = [];
    this.stacksList.forEach((stack) => {
      if (stack.getStatus() === 1) {
        requests.push(this.stackStore.stopStack(stack.getID()));
      }
    });
    scheduled(requests, queueScheduler).pipe(concatAll()).subscribe();
  }

  /**
   * Delete all existing stacks
   */
  onDelAll(): void {
    this.dialogService
      .open(DialogNamePromptComponent, {
        context: {
          message: `Cette action supprimera définitivement toutes les sites ${this.type} et les données associées`,
        },
      })
      .onClose.subscribe((name) => {
        if (name === "oui") {
          this.toast.info(`Tous les sites ${this.type} vont être supprimés`);
          const requests = [];
          this.stacksList.forEach((stack) => {
            requests.push(
              this.stackStore.deleteStack(stack.getID(), stack.getName())
            );
          });
          scheduled(requests, queueScheduler).pipe(concatAll()).subscribe();
        }
      });
  }

  /**
   * Generate PDF file with site URLs
   */
  onGetPdf(): void {
    const listURL = [];
    let passwd = "";
    this.stacksList.forEach((stack) => {
      if (stack.getStatus() == 1) {
        if (this.type === SERVER_TYPES.mautic) {
          passwd = stack.getEnvVars().find((e) => e.name === "DB_PASS").value;
        }

        listURL.push({
          site: stack.getMainContainer().getName(),
          url: stack.getMainContainer().getUrlBackOffice(),
          mdpMautic: passwd,
        });
      }
    });
    this.dialogService.open(ShowcaseDialogComponent, {
      context: {
        title: "Liste des sites " + this.type + " avec leur URL",
        listURL: listURL,
        typeSite: this.type,
      },
    });
  }

  /**
   *
   * @param {string} name
   * @param {string} stackID
   * @param {boolean} state
   * @param {string} serverType
   */
  buildServerCard(name: string, stackID: number, state: boolean): void {
    const serverCard: CardSettings = {
      title: name,
      stackID: stackID,
      iconClass: "nb-power-circled",
      type: "info",
      on: state,
    };

    this.commonStatusCardsSet.push(serverCard);
  }

  /**
   *
   * @param {string} suffix
   * @return {string}
   */
  private buildNameToDisplay(suffix: string): string {
    return this.type + "-" + suffix;
  }

  /**
   *
   */
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
