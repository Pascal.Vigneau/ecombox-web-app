import { NgModule } from "@angular/core";
import { ThemeModule } from "../../@theme/theme.module";
import { TranslateModule } from "@ngx-translate/core";
import { PolicyComponent } from "./policies.component";
import { NbCardModule } from "@nebular/theme";

@NgModule ({
    imports: [
        NbCardModule,
        ThemeModule,
        TranslateModule.forChild({
        extend: true,
        }),
    ],
    declarations: [
        PolicyComponent,
    ],
})
export class PolicyModule {}