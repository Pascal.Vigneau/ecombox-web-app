import { Component } from "@angular/core";

@Component({
  selector: "ngx-dashboard",
  styleUrls: ["./dashboard.component.scss"],
  templateUrl: "./dashboard.component.html",
})
export class DashboardComponent {
  /**
   * No Param - void return
   */
  constructor() {
    console.info("e-comBox v3.0.4 du 07/02/2022");
  }
}
