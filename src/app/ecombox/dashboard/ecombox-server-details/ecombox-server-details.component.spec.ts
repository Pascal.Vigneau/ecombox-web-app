import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EcomboxServerDetailsComponent } from './ecombox-server-details.component';

describe('EcomboxServerDetailsComponent', () => {
  let component: EcomboxServerDetailsComponent;
  let fixture: ComponentFixture<EcomboxServerDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcomboxServerDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcomboxServerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
