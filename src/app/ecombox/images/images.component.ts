/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { Component } from "@angular/core";
import { LocalDataSource } from "ng2-smart-table";
import { ToastrService } from "ngx-toastr";
import { DockerRegistryService } from "../shared/api/docker-registry.service";
import { StackStore } from "../shared/store/stack.store";

@Component({
  selector: "ecx-images",
  styleUrls: ["./images.component.scss"],
  templateUrl: "./images.component.html",
})
export class ImagesComponent {
  public source = new LocalDataSource();

  settings = {
    hideSubHeader: true,
    actions: {
      add: false,
      edit: false,
    },
    rowClassFunction: (row) => {
      // console.log("row.data.type:: " + row.data.type);
      return row.data.type;
    },
    add: {
      addButtonContent: "",
      createButtonContent: "",
      cancelButtonContent: "",
    },
    edit: {
      editButtonContent: "",
      saveButtonContent: "",
      cancelButtonContent: "",
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      tag: {
        title: "Nom du modèle",
        type: "string",
      },
      type: {
        title: "Type de modèle de site",
        type: "string",
      },
      /* stacks: {
        title: "Nombre de sites créés à partir de ce modèle",
        type: "string",
      },*/
    },
  };

  data = [];

  /**
   *
   * @param {DockerRegistryService} registryService
   */
  constructor(
    private registryService: DockerRegistryService,
    private store: StackStore,
    private toastr: ToastrService
  ) {
    this.registryService
      .getAllTags(this.store.getDockerIP(), this.store.getNginxPort())
      .subscribe((result) => {
        result.forEach((repo) => {
          if(repo.tags){
          repo.tags.forEach((tag: string) => {
            this.data.push({
              tag: tag,
              type: repo.name,
            });
          });
        }});
        this.source.load(this.data);
      });
  }

  /**
   *
   * @param {Event} event
   */
  onDeleteConfirm(event): void {
    // console.dir(event);
    if (window.confirm("Êtes-vous sûr de vouloir supprimer ce modèle ?")) {
      this.registryService
        .registryDelImage(
          event.data.type,
          event.data.tag,
          this.store.getDockerIP(),
          this.store.getNginxPort()
        )
        .subscribe((ref) => {
          this.toastr.success(
            "Le modèle " + event.data.tag + " a été supprimé."
          );
        });
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
