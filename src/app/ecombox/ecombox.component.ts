import { Component, OnInit } from "@angular/core";

import { MENU_ECOMBOX_ITEMS } from "./ecombox-menu";
import { AuthService } from "./shared/api/auth.service";

@Component({
  selector: "ngx-ecombox",
  templateUrl: "./ecombox.component.html",
  styleUrls: ["./ecombox.component.scss"],
})
export class EcomboxComponent implements OnInit {
  menu = MENU_ECOMBOX_ITEMS;

  /**
   *
   * @param {StackStore} stackStore Class to manage stakcs
   * @param {AuthService} authService Authentication service
   */
  constructor(private authService: AuthService) {}

  /**
   * ngOnInit(): void
   */
  ngOnInit(): void {
    // Authentication for Portainer API
    this.authService.authenticateUser().subscribe();
  }
}
