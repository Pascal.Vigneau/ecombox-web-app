**Lien vers les "issues"** : https://gitlab.com/groups/e-combox/-/issues

# V4

C'est la version qui va permettre de déployer l'e-comBox à grande échelle dans les régions. Elle permettra notamment :
- l'authentification des professeurs multi-utilisateurs (sur une instance e-comBox partagée, les professeurs ne verront et ne pouront intéragir que sur les sites qu'ils ont eux-mêmes déployés) avec à terme une authentification via l'ammuaire centralisateur ;
- le passage par un reverse proxy qui permettra de s'affranchir de tous les ports ;
- la bascule de HTTP en HTTPS.

## 4.1.0

### Fonctionnalités

**Authentification** : Module permettant l'authentification des professeurs sur l'interface via l'annuaire centralisateur (avec éventuellement une intégration au GAR).
- **Partager ses propres modèles de sites** : le partage de modèles de sites sera possible directement à partir de l'interface e-comBox après avoir créé un compte sur la plateforme Docker Hub.
- **Récupérer un modèle de site créé par un utilisateur de la communauté** : il sera désormais possible, à partir du nom complet du modèle, de créer un site à partir d'un modèle partagé par un utilisateur de la communauté e-comBox.
- 

## 4.0.0

### Fonctionnalités

- **Reverse Proxy** : Possibilité de passer par un Reverse Proxy de manière à ne pas être obligé d'exposer des ports sur le serveur. 
- **Authentification** : Module permettant l'authentification des professeurs sur l'interface via un système d'authentification multi-utilisateurs interne à e-comBox.


### Sécurisation

- Intégration du protocole HTTPS au niveau des sites et de toute l'application (Portainer et e-comBox).



# V3

## Pour toutes les versions

- **kanboard** : mise à jour vers la version 1.2.21, intégration des plugins et possibilité de les installer via l'interface.

### Sites

- **Tous les types de site** : mise à jour des versions.
- **Général** : sauvegarde/restauration d'un seul site (en demandant un chemin à l’utilisateur).
- **Prestashop** : intégration d'un module tchat sur Prestashop.
- **Odoo** : Ajout de trois nouvelles bases : AdA, Générik et Pépinières.
- **Kanboard** : Ajout d'une nouvelle base : BdDev.

### Fonctionnalités

- **Gérer vos modèles de site** : possibilité de renommer les images personnalisées par les profs (**V3.1**).
- \*\*Sauvegarde et restauration d'un seul site.
- **En bas du menu de gauche** (sous Aide) : Intégration de CGU personnalisables (**V3.1**).
- **Mise à jour sur Linux** : automatiser les mises à jour (aucune action nécessaire pour l'administrateur sauf à lancer la mise à jour)
